package com.bytecub.openapi.controller;

import com.bytecub.common.constants.BCConstants;
import com.bytecub.common.domain.DataResult;
import com.bytecub.common.domain.dto.PageReqDto;
import com.bytecub.common.domain.dto.PageResDto;
import com.bytecub.common.domain.dto.request.DevQueryReqDto;
import com.bytecub.common.domain.dto.request.product.ProductQueryReqDto;
import com.bytecub.common.domain.dto.request.prop.TemplateReqDto;
import com.bytecub.common.domain.dto.response.ProductResDto;
import com.bytecub.common.domain.dto.response.device.DevicePageResDto;
import com.bytecub.common.domain.dto.response.prop.TemplateResDto;
import com.bytecub.common.enums.BCErrorEnum;
import com.bytecub.common.exception.BCGException;
import com.bytecub.mdm.service.IDeviceService;
import com.bytecub.mdm.service.IProductFuncService;
import com.bytecub.mdm.service.IProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
  * ByteCub.cn.
  * Copyright (c) 2020-2021 All Rights Reserved.
  * 
  * @author bytecub@163.com  songbin
  * @version Id: OpenDeviceController.java, v 0.1 2021-01-08  Exp $$
  */
@Slf4j
@RestController
@RequestMapping(BCConstants.URL_PREFIX.OPEN_API + "product")
@Api(description = "开放平台产品管理")
public class OpenProductController {
    @Autowired
    IProductService productService;

    @RequestMapping(value = "search", method = RequestMethod.POST)
    @ApiOperation(value = "分页查询产品列表", httpMethod = "POST", response = DataResult.class, notes = "分页查询产品列表")
    public DataResult<PageResDto<ProductResDto>> search(@RequestBody PageReqDto<String> searchPage){
        PageResDto<ProductResDto> page = productService.searchByName(searchPage);
        return DataResult.ok(page);
    }


    @RequestMapping(value = "query", method = RequestMethod.POST)
    @ApiOperation(value = "结构体一次列出所有产品", httpMethod = "POST", response = DataResult.class, notes = "结构体一次列出所有产品")
    public DataResult<List<ProductResDto>> query(@RequestBody ProductQueryReqDto query){
        List<ProductResDto> result = productService.query(query);
        return DataResult.ok(result);
    }

    @RequestMapping(value = "multiple", method = RequestMethod.POST)
    @ApiOperation(value = "根据多个产品编码查询产品信息", httpMethod = "POST", response = DataResult.class, notes = "结构体一次列出所有产品")
    public DataResult<List<ProductResDto>> multiple(@RequestBody List<String> productCodes){
        List<ProductResDto> result = productService.multiple(productCodes);
        return DataResult.ok(result);
    }
}
